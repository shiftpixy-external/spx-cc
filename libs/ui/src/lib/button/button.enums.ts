export enum Colors {
  White = 'white',
  Light = 'light',
  Dark = 'dark',
  Black = 'black',
  Text = 'text',
  Ghost = 'ghost',
  Primary = 'primary',
  Link = 'link',
  Info = 'info',
  Success = 'success',
  Warning = 'warning',
  Danger = 'danger',
}

export enum Sizes {
  Small = 'small',
  Normal = 'normal',
  Medium = 'medium',
  Large = 'large',
}

export enum Styles {
  None = '',
  Outlined = 'outlined',
  Inverted = 'inverted',
  Rounded = 'rounded',
}
