import { Component } from '@angular/core'
import { HttpClient } from '@angular/common/http'
import { Card } from '@spx-cc/api-interfaces'
import { Colors, Sizes } from '@spx-cc/ui'

@Component({
  selector: 'spx-cc-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  cards$ = this.http.get<Card[]>('/api/cards')
  Colors = Colors
  Sizes = Sizes
  constructor(private http: HttpClient) {}
}
