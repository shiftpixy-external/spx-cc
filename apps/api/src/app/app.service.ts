import { Injectable } from '@nestjs/common'
import { Card } from '@spx-cc/api-interfaces'

@Injectable()
export class AppService {
  getData(): Card[] {
    return [
      {
        id: 1,
        title: 'First Card',
        description: 'This is the first card to display',
        url: new URL('https://nottinghamsheriff.app'),
      },
      {
        id: 2,
        title: 'Second Card',
        description: 'This is the second card to display',
        url: new URL('https://nottinghamsheriff.app'),
        ctaText: 'Learn More',
      },
    ]
  }
}
